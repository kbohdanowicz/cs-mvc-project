﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using ProjectMVC.Validation;

namespace ProjectMVC.Models
{
    public class Product
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Name must have at least {2} character")] 
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Category]
        [Display(Name = "Category")]
       
        public string category { get; set; }
        [Required]
        [Range(25, 5000, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        [DisplayFormat(DataFormatString = "{0:#g}")]
        [Display(Name = "Weight")]
        public int weight { get; set; }

        [Required]
        [Range(0, 1000)]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:$0.##}")]
        [Display(Name = "Price")]
        public decimal price { get; set; }

        [Display(Name = "Is on sale")]
        public bool isSale { get; set; }

        [Required]
        [Display(Name = "Manufacturer ID")]
        public int manufacturerId { get; set; }

        [Display(Name = "Manufacturer")]
        public virtual Manufacturer manufacturer{ get; set; }
    }
}
