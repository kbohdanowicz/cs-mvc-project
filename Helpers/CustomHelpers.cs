﻿using System;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace ProjectMVC.Helpers {
    public class CustomHelpers {
        public static HtmlString NavLink(string target, string name) {
            return new HtmlString(String.Format(
                "<li class='nav-item '>" +
                "   <a class='nav-link text-white' href='{0}'>{1}</a>" +
                "</li>"
                , target, name));
        }

        public static HtmlString Create(string target) {
            return new HtmlString(String.Format(
                "<p>" +
                "   <a class=\"btn-success btn-sm btn\" href='/{0}/Create'>Create New</a>" +
                "</p>"
                , target));
        }
       
        public static HtmlString Details(string target, int id) {
            return new HtmlString(String.Format(
                "<a class=\"btn-info btn-sm btn\" href='/{0}/Details/{1}'>Details</a> &nbsp;"
                , target, id));
        }

        public static HtmlString Edit(string target, int id) {
            return new HtmlString(String.Format(
                "<a class=\"btn-warning btn-sm btn\" href='/{0}/Edit/{1}'>Edit</a> &nbsp;"
                , target, id));
        }

        public static HtmlString Control(string target, int id) {
            return new HtmlString(String.Format(
                "<a class=\"btn-warning btn-sm btn\" href='/{0}/Edit/{1}'>Edit</a> &nbsp;" +
                "<a class=\"btn-danger btn-sm btn\" href='/{0}/Delete/{1}'>Delete</a>"
                , target, id));
        }
    }
}
