﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectMVC.Validation {
    public class Category : ValidationAttribute {

        enum CategoryEnum { Tea, Yerba, Coffee, Accessories, Misc }
        
        protected override ValidationResult IsValid(object value, ValidationContext validationContext) {
            if (value != null) {
                string categoryInput = value.ToString();

                foreach (CategoryEnum ce in Enum.GetValues(typeof(CategoryEnum))) {
                    if (ce.ToString().Equals(categoryInput)) 
                        return ValidationResult.Success;
                }
            }
            return new ValidationResult("Invalid category");
        }
    } 
}
